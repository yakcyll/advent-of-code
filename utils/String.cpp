#include <algorithm>
#include <array>
#include <filesystem>
#include <iostream>
#include <iomanip>
#include <ios>
#include <regex>
#include <sstream>

#include <cmath>
#include <cstring>
#include <cuchar>

#include "StringCaps.hpp"
#include "StringUtils.hpp"
#include "String.hpp"

using CharacterType = String::CharacterType;

CharacterType *String::Ptr()
{
    return _data.data();
}

void String::RemoveAt(const size_t index)
{
    _data.erase(_data.begin() + static_cast<StringType::difference_type>(index));
}

void String::Clear()
{
    _data.clear();
}

CharacterType String::Get(const ssize_t index) const
{
    if ((index >= Size()) && (index <= -Size()))
    {
        return _null;
    }

    ssize_t actualIndex = index;

    if (actualIndex < 0)
    {
        actualIndex += Size();
    }

    return _data[actualIndex];
}

void String::Set(const size_t index, const CharacterType &elem)
{
    _data[index] = elem;
}

size_t String::Size() const
{
    return _data.size() + 1U; // String length is kept track of separately from the string itself!
}

void String::Resize(const size_t newSize)
{
    return _data.resize(newSize);
}

const CharacterType &String::operator[](const size_t index) const
{
    if (index >= Size())
    {
        return _null;
    }

    return _data[index];
}

bool String::operator==(const String &other) const
{
    return _data == other._data;
}

bool String::operator!=(const String &other) const
{
    return _data != other._data;
}

String String::operator+(const String &other) const
{
    String retval = {};
    retval._data = _data + other._data;
    return retval;
}

String String::operator+(const char32_t character) const
{
    String retval = {};
    retval._data = _data + character;
    return retval;
}

String &String::operator+=(const String &other)
{
    _data += other._data;
    return *this;
}

String &String::operator+=(const char32_t character)
{
    _data += character;
    return *this;
}

String &String::operator+=(const char *other)
{
    if ((other == nullptr) || other[0] == 0)
    {
        return *this;
    }

    const size_t thisLength = Length();
    const size_t otherLength = ::strnlen_s(other, 1024U);

    Resize(thisLength + otherLength + 1U);

    CharacterType *copyPtr = Ptr() + thisLength;

    for (size_t i = 0U; i <= otherLength; ++i)
    {
        uint8_t c;
        if (other[i] >= 0)
        {
            c = other[i];
        }
        else
        {
            c = static_cast<uint8_t>(256 + other[i]);
        }

        if ((c == 0) && (i < otherLength))
        {
            copyPtr[i] = _replacementCharacter;
        }
        else
        {
            copyPtr[i] = c;
        }
    }

    return *this;
}

String &String::operator+=(const wchar_t *other)
{
    // #ifdef CH_PLATFORM_WINDOWS
    *this += String::Utf16(reinterpret_cast<const char16_t *>(other));
    // #else
    // *this += String(reinterpret_cast<const char32_t*>(other));
    // #endif

    return *this;
}

String &String::operator+=(const char32_t *other)
{
    *this += String(other);
    return *this;
}

String &String::operator+=(const std::string &other)
{
    *this += String(other);
    return *this;
}

String &String::operator+=(const StringType &other)
{
    *this += String(other);
    return *this;
}

String &String::operator=(const char *other)
{
    _CopyFrom(other);
    return *this;
}

String &String::operator=(const wchar_t *other)
{
    _CopyFrom(other);
    return *this;
}

String &String::operator=(const char32_t *other)
{
    _CopyFrom(other);
    return *this;
}

String &String::operator=(const std::string &other)
{
    _CopyFrom(other);
    return *this;
}

String &String::operator=(const StringType &other)
{
    _CopyFrom(other);
    return *this;
}

bool String::operator==(const char *other) const
{
    return *this == String(other);
}

bool String::operator==(const wchar_t *other) const
{
    return *this == String(other);
}

bool String::operator==(const char32_t *other) const
{
    return *this == String(other);
}

bool String::operator==(const std::string &other) const
{
    return *this == String(other);
}

bool String::operator==(const StringType &other) const
{
    return *this == String(other);
}

bool String::operator!=(const char *other) const
{
    return *this != String(other);
}

bool String::operator!=(const wchar_t *other) const
{
    return *this != String(other);
}

bool String::operator!=(const char32_t *other) const
{
    return *this != String(other);
}

bool String::operator!=(const std::string &other) const
{
    return *this != String(other);
}

bool String::operator!=(const StringType &other) const
{
    return *this != String(other);
}

bool String::operator<(const char *other) const
{
    return *this < String(other);
}

bool String::operator<(const wchar_t *other) const
{
    return *this < String(other);
}

bool String::operator<(const char32_t *other) const
{
    return *this < String(other);
}

bool String::operator<(const std::string &other) const
{
    return *this < String(other);
}

bool String::operator<(const StringType &other) const
{
    return *this < String(other);
}

bool String::operator<(const String &other) const
{
    return _data < other._data;
}

bool String::operator<=(const String &other) const
{
    return _data <= other._data;
}

bool String::operator>(const String &other) const
{
    return _data > other._data;
}

bool String::operator>=(const String &other) const
{
    return _data >= other._data;
}

signed char String::Compare(const String &other, const bool caseInsensitive) const
{
    if (!caseInsensitive)
    {
        return _data.compare(other._data);
    }
    else
    {
        String usUpper = ToUpper();
        String themUpper = other.ToUpper();
        return usUpper.Compare(themUpper);
    }
}

const CharacterType *String::Data() const
{
    return _data.data();
}

size_t String::Length() const
{
    const size_t s = Size();
    return (s > 0U) ? (s - 1U) : 0U; // length does not include zero
}

bool String::IsValidString() const
{
    static constexpr CharacterType UNICODE_SURROGATE_LOWEST_CODE_POINT = 0xD800U;
    static constexpr CharacterType UNICODE_SURROGATE_HIGHEST_CODE_POINT = 0xDFFFU;
    static constexpr CharacterType UNICODE_HIGHEST_CODE_POINT = 0x10FFFFU;

    const size_t length = Length();
    const CharacterType *data = Data();
    for (size_t i = 0U; i < length; ++i)
    {
        if (
            ((data[i] > UNICODE_SURROGATE_LOWEST_CODE_POINT) && (data[i] < UNICODE_SURROGATE_HIGHEST_CODE_POINT)) || (data[i] > UNICODE_HIGHEST_CODE_POINT))
        {
            return false;
        }
    }

    return true;
}

void String::PrintUnicodeError(const String &message, const bool critical) const
{
    if (critical)
    {
        std::cerr << "Unicode parsing error, some characters were replaced with (U+FFFD): " << message.Ascii() << std::endl;
    }
    else
    {
        std::cerr << "Unicode parsing error: " << message.Ascii() << std::endl;
    }
}

size_t String::Count(const String &string, const size_t from, const ssize_t to, const bool caseInsensitive) const
{
    if (caseInsensitive)
    {
        String usUpper = ToUpper();
        String stringUpper = string.ToUpper();
        return usUpper.Count(stringUpper, from, to);
    }

    String searchRange = Substring(from, to);
    size_t stringLength = string.Length();
    size_t count = 0U;
    StringType::size_type pos = 0U;
    while ((pos = searchRange._data.find(string._data, pos)) != StringType::npos)
    {
        ++count;
        pos += stringLength;
    }

    return count;
}

String String::Substring(const size_t from, const ssize_t count) const
{
    if (count < 0)
    {
        return _data.substr(from);
    }
    return _data.substr(from, static_cast<size_t>(count));
}

ssize_t String::Find(const String &other, const size_t from, const bool caseInsensitive) const
{
    if (caseInsensitive)
    {
        String usUpper = ToUpper();
        String otherUpper = other.ToUpper();
        return usUpper.Find(otherUpper);
    }

    return static_cast<ssize_t>(_data.find(other._data, from));
}

ssize_t String::Find(const char *other, const size_t from) const
{
    return static_cast<ssize_t>(_data.find(ConvertStringTypes<char, CharacterType>(std::string(other)), from));
}

ssize_t String::FindCharacter(const CharacterType &character, const size_t from) const
{
    return static_cast<ssize_t>(_data.find(character, from));
}

ssize_t String::ReverseFind(const String &other, const ssize_t from, const bool caseInsensitive) const
{
    if (caseInsensitive)
    {
        String usUpper = ToUpper();
        String otherUpper = other.ToUpper();
        return usUpper.ReverseFind(otherUpper);
    }

    return static_cast<ssize_t>(_data.rfind(other._data, from));
}

bool String::Match(const String &pattern, const bool caseInsensitive) const
{
    const std::regex_constants::syntax_option_type flags = caseInsensitive ? std::regex_constants::icase : std::regex_constants::syntax_option_type{};
    const std::regex matchRegex(ConvertStringTypes<CharacterType, char>(pattern._data).c_str(), flags);
    return std::regex_search(ConvertStringTypes<CharacterType, char>(_data), matchRegex);
}

bool String::BeginsWith(const String &other) const
{
    return _data.starts_with(other._data);
}

bool String::BeginsWith(const char *other) const
{
    return _data.starts_with(String(other)._data);
}

bool String::EndsWith(const String &other) const
{
    return _data.ends_with(other._data);
}

bool String::IsEnclosedIn(const String &other) const
{
    return _data.starts_with(other._data) && _data.ends_with(other._data);
}

bool String::IsQuoted() const
{
    return IsEnclosedIn("\"") || IsEnclosedIn("'");
}

String String::ReplaceFirst(const String &what, const String &with) const
{
    StringType::size_type pos = _data.find(what._data);
    if (pos >= 0)
    {
        return _data.substr(0U, pos) + with._data + _data.substr(pos + what.Length(), Length());
    }

    return *this;
}

String String::Replace(const String &what, const String &with, const bool caseInsensitive) const
{
    String retval = *this;
    StringType::size_type pos{};
    while ((pos = retval.Find(what, 0U, caseInsensitive)) != String::npos)
    {
        retval = String(retval._data.substr(0U, pos) + with._data + retval._data.substr(pos + what.Length(), retval.Length()));
    }
    return retval;
}

String String::Replace(const char *what, const char *with) const
{
    return Replace(String(what), String(with));
}

String String::Repeat(const size_t count) const
{
    if (count == 0U)
    {
        return "";
    }

    if (count == 1U)
    {
        return *this;
    }

    String retval = {};
    retval._data = {};

    for (size_t i = 0U; i < count; ++i)
    {
        retval._data += _data;
    }

    return retval;
}

String String::Reverse() const
{
    String retval = *this;
    std::reverse(retval._data.begin(), retval._data.end());
    return retval;
}

String String::Insert(const size_t position, const String &string) const
{
    return _data.substr(0U, position) + string._data + _data.substr(position, _data.length());
}

String String::Erase(const size_t position, const size_t count) const
{
    return _data.substr(0U, position) + _data.substr(position + count, _data.length());
}

String String::PadDecimals(const size_t digits) const
{
    String retval = *this;
    StringType::size_type decimalPosition = Find(".");

    if (decimalPosition == String::npos)
    {
        if (digits == 0U)
        {
            return retval;
        }
        else
        {
            retval += ".";
            decimalPosition = retval.Length() - 1U;
        }
    }
    else
    {
        if (digits == 0U)
        {
            return retval.Substring(0, decimalPosition);
        }
    }

    const size_t decimalDigitCount = retval.Length() - decimalPosition - 1U;
    if (digits < decimalDigitCount)
    {
        return Substring(0U, decimalPosition + digits + 1U);
    }
    else
    {
        const size_t zeroesToAdd = decimalDigitCount - digits;
        return retval + String("0").Repeat(zeroesToAdd);
    }
}

String String::PadZeros(const size_t digits) const
{
    ssize_t end = Find(".");

    if (end == String::npos)
    {
        end = static_cast<ssize_t>(_data.length());
    }

    const ssize_t begin = std::find_if(_data.begin(), _data.end(), IsDigit<CharacterType>) - _data.begin();
    const ssize_t zeroesToAdd = static_cast<ssize_t>(digits) - (end - begin);
    if (zeroesToAdd <= 0)
    {
        return *this;
    }
    else
    {
        return Insert(static_cast<size_t>(begin), String("0").Repeat(static_cast<size_t>(zeroesToAdd)));
    }
}

String String::TrimPrefix(const String &prefix) const
{
    if (BeginsWith(prefix))
    {
        return Substring(prefix.Length(), static_cast<ssize_t>(Length() - prefix.Length()));
    }

    return *this;
}

String String::TrimSuffix(const String &suffix) const
{
    if (EndsWith(suffix))
    {
        return Substring(0U, static_cast<ssize_t>(Length() - suffix.Length()));
    }

    return *this;
}

String String::LeftPad(const size_t minLength, const String &character) const
{
    if (minLength > Length())
    {
        return character.Repeat(minLength - Length()) + *this;
    }

    return *this;
}

String String::RightPad(const size_t minLength, const String &character) const
{
    if (minLength > Length())
    {
        return *this + character.Repeat(minLength - Length());
    }

    return *this;
}

String String::Quote(const String &quoteCharacter) const
{
    return quoteCharacter + *this + quoteCharacter;
}

String String::Unquote() const
{
    if (!IsQuoted())
    {
        return *this;
    }

    return Substring(1U, Length() - 2U);
}

String String::FromDouble(const double number, const ssize_t decimals)
{
    std::stringstream dtos;
    dtos << std::setfill('0') << std::setprecision(decimals) << number;
    return dtos.str();
}

String String::FromDoubleScientific(const double number)
{
    std::stringstream dtos;
    dtos << std::scientific << number;
    return dtos.str();
}

template <typename T>
T customAbs(const T in)
{
    if (in < 0)
    {
        return -1 * in;
    }

    return in;
}

String String::FromInt64(const int64_t number, const size_t base, const bool capitalizeHex)
{
    static const std::array<CharacterType, 35U> chars = {{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                                                          'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                                                          'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u',
                                                          'v', 'w', 'x', 'y', 'z'}};

    if (base > chars.size())
    {
        return "";
    }

    int64_t reminder = number;
    String sign = (number < 0) ? "-" : "";
    String retval{};

    if (number < 0)
    {
        retval += "-";
    }

    while (reminder > base)
    {
        retval += chars[customAbs(reminder % base)];
        reminder /= base;
    }

    if (capitalizeHex)
    {
        retval = retval.ToUpper();
    }

    return sign + retval.Reverse();
}

String String::FromUint64(const uint64_t number, const size_t base, const bool capitalizeHex)
{
    static const std::array<CharacterType, 35U> chars = {{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                                                          'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                                                          'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u',
                                                          'v', 'w', 'x', 'y', 'z'}};

    if (base > chars.size())
    {
        return "";
    }

    uint64_t reminder = number;
    String retval{};

    while (reminder > base)
    {
        retval += chars[customAbs(reminder % base)];
        reminder /= base;
    }

    if (capitalizeHex)
    {
        retval = retval.ToUpper();
    }

    return retval.Reverse();
}

String String::FromChar32(const CharacterType character)
{
    std::array<CharacterType, 2U> tmp = {character, 0};
    return String(tmp.data());
}

String String::HexEncodeFromBuffer(const uint8_t *buffer, const size_t length)
{
    static const std::array<CharacterType, 16U> chars = {{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                                                          'a', 'b', 'c', 'd', 'e', 'f'}};

    String retval = {};

    for (size_t i = 0U; i < length; ++i)
    {
        uint8_t byte = buffer[i];
        retval += chars[byte / 16U];
        retval += chars[byte % 16U];
    }

    return retval;
}

std::vector<uint8_t> String::HexDecode() const
{
    std::vector<uint8_t> retval;
    if ((Length() % 2U) != 0U)
    {
        throw std::runtime_error("Trying to hex decode a string of an uneven length!");
    }

    for (size_t i = 0U; i < Length(); i += 2)
    {
        uint8_t byte = 0;
        for (size_t j = 0U; j < 2; ++j)
        {
            uint8_t num = 0;
            const CharacterType chr = _data[i + j];
            if ((chr >= '0') && (chr <= '9'))
            {
                num = (chr - '0');
            }
            else if ((chr >= 'a') && (chr <= 'f'))
            {
                num = (chr - 'a');
            }
            else if ((chr >= 'A') && (chr <= 'F'))
            {
                num = (chr - 'A');
            }
            else
            {
                throw std::runtime_error("Trying to hex decode a string with non-hex characters!");
            }

            byte += num * static_cast<uint8_t>(std::pow(16, 1 - j));
        }

        retval.push_back(byte);
    }

    return retval;
}

bool String::IsNumeric() const
{
    if (Length() == 0U)
    {
        return false;
    }

    const size_t dotCount = Count(".");

    if (dotCount > 1U)
    {
        return false;
    }

    const size_t nonNumericCharacterRangeStart = (_data[0] == '-') ? 1U : 0U;
    const size_t nonNumericCharacters = std::count_if(_data.begin() + nonNumericCharacterRangeStart, _data.end(), ::IsNotDigit<CharacterType>);
    return ((nonNumericCharacters - dotCount) == 0U);
}

double String::ToFloat() const
{
    return std::stod(ConvertStringTypes<CharacterType, char>(_data));
}

int64_t String::HexToInt() const
{
    String lowerString = ToLower();
    static int64_t sign = (lowerString[0] == '-') ? -1 : 1;

    if (lowerString[0] == '-')
    {
        lowerString = lowerString.Substring(1U);
    }

    if (lowerString.Find("0x") == 0U)
    {
        lowerString = lowerString.Substring(2U);
    }

    if (std::count_if(lowerString._data.begin(), lowerString._data.end(), ::IsNotHex<CharacterType>) != 0)
    {
        throw std::runtime_error("Cannot turn a string with non-hex characters into a number!");
    }

    static constexpr size_t LOG_16_INT64_MAX = 15U;
    if (lowerString.Length() > LOG_16_INT64_MAX)
    {
        throw std::runtime_error("String too long to fit into an int64_t!");
    }

    int64_t retval = 0U;

    for (auto &chr : lowerString._data)
    {
        int64_t factor = 0U;
        if (IsDigit<>(chr))
        {
            factor = static_cast<int64_t>(chr - '0');
        }
        else if (IsHex<>(chr))
        {
            factor = static_cast<int64_t>(chr - 'a');
        }
        retval *= 16U;
        retval += factor;
    }

    return sign * retval;
}

int64_t String::BinToInt() const
{
    String lowerString = ToLower();
    static int64_t sign = (lowerString[0] == '-') ? -1 : 1;

    if (lowerString[0] == '-')
    {
        lowerString = lowerString.Substring(1U);
    }

    auto binaryIndicatorPosition = lowerString.Find("b");
    if (binaryIndicatorPosition == 0U)
    {
        lowerString = lowerString.Substring(1U);
    }
    else if (binaryIndicatorPosition == (Length() - 1U))
    {
        lowerString = lowerString.Substring(0, Length() - 1U);
    }

    if (std::count_if(lowerString._data.begin(), lowerString._data.end(), ::IsNotBinaryDigit<CharacterType>) != 0)
    {
        throw std::runtime_error("Cannot turn a string with non-binary characters into a number!");
    }

    static constexpr size_t LOG_2_INT64_MAX = 63U;
    if (lowerString.Length() > LOG_2_INT64_MAX)
    {
        throw std::runtime_error("String too long to fit into an int64_t!");
    }

    int64_t retval = 0U;

    for (auto &chr : lowerString._data)
    {
        int64_t factor = static_cast<int64_t>(chr - '0');
        retval *= 2U;
        retval += factor;
    }

    return sign * retval;
}

int64_t String::ToInt() const
{
    return std::stoi(ConvertStringTypes<CharacterType, char>(_data));
}

int64_t String::ToInt(const char *string, const ssize_t length)
{
    return std::stoi(std::string(string, length));
}

int64_t String::ToInt(const wchar_t *string, const ssize_t length)
{
    return std::stoi(std::wstring(string, length));
}

int64_t String::ToInt(const CharacterType *string, const ssize_t length, const bool clamp)
{
    if (length <= 0)
    {
        return 0U;
    }

    int64_t retval;
    const int64_t sign = (string[0] == '-') ? -1 : 1;

    try
    {
        retval = std::stoi(ConvertStringTypes<CharacterType, char>(String(string, length)._data));
    }
    catch (std::out_of_range &e)
    {
        if (clamp)
        {
            return (sign == 1) ? INT64_MAX : INT64_MIN;
        }
        else
        {
            throw;
        }
    }
    return retval;
}

double String::ToFloat(const char *string, const ssize_t length)
{
    return std::stod(std::string(string, length));
}

double String::ToFloat(const wchar_t *string, const ssize_t length)
{
    return std::stod(std::wstring(string, length));
}

double String::ToFloat(const CharacterType *string, const ssize_t length)
{
    return std::stod(ConvertStringTypes<CharacterType, char>(String(string, length)._data));
}

String String::Capitalize() const
{
    String aux = _CamelcaseToUnderscore().Replace("_", " ").StripEdges();
    auto slices = aux.SplitSpaces();
    for (auto &slice : slices)
    {
        slice.Set(0U, FindUpperCase(slice[0]));
    }

    return String(" ").Join(slices);
}

String String::ToCamelCase() const
{
    String retval = ToPascalCase();
    if (!retval.IsEmpty())
    {
        retval.Set(0U, FindLowerCase(retval[0]));
    }

    return retval;
}

String String::ToPascalCase() const
{
    return Capitalize().Replace(" ", "");
}

String String::ToSnakeCase() const
{
    return _CamelcaseToUnderscore().Replace(" ", "_").StripEdges();
}

size_t String::GetSliceCount(const String delimiter) const
{
    if (IsEmpty() || delimiter.IsEmpty())
    {
        return 0U;
    }

    return Count(delimiter) + 1U;
}

String String::GetSlice(const String delimiter, const size_t sliceIdx) const
{
    size_t remainingSlices = sliceIdx;
    size_t startPos = 0U;
    size_t endPos = 0U;

    while (true)
    {
        endPos = Find(delimiter, startPos);
        if (endPos != String::npos)
        {
            if (remainingSlices > 0U)
            {
                startPos = endPos + delimiter.Length();
            }
            else
            {
                return Substring(startPos, endPos - startPos);
            }
        }
        else
        {
            if (remainingSlices > 0U)
            {
                return "";
            }
            else
            {
                return Substring(startPos, endPos - startPos);
            }
        }

        remainingSlices -= 1U;
    }
}

String String::GetSliceByCharacter(const CharacterType delimiter, const size_t sliceIdx) const
{
    const std::array<CharacterType, 2U> delimiterStr = {{delimiter, 0}};
    return GetSlice(String(delimiterStr.data()), sliceIdx);
}

std::vector<String> String::Split(const String &delimiter, const bool allowEmpty, const size_t maxSplitCount) const
{
    std::vector<String> retval = {};

    const size_t sliceCount = std::min(maxSplitCount, GetSliceCount(delimiter));

    for (size_t i = 0U; i < sliceCount; ++i)
    {
        auto slice = GetSlice(delimiter, i);
        if ((slice.Length() == 0U) && !allowEmpty)
        {
            continue;
        }

        retval.push_back(slice);
    }

    return retval;
}

std::vector<String> String::ReverseSplit(const String &delimiter, const bool allowEmpty, const size_t maxSplitCount) const
{
    auto slices = Split(delimiter, allowEmpty, maxSplitCount);
    std::reverse(slices.begin(), slices.end());
    return slices;
}

std::vector<String> String::SplitSpaces() const
{
    return Split(" ");
}

std::vector<double> String::SplitFloats(const String &delimiter, const bool allowEmpty) const
{
    auto slices = Split(delimiter, allowEmpty);

    std::vector<double> floats = {};

    for (auto &slice : slices)
    {
        floats.push_back(slice.ToFloat());
    }
    return floats;
}

std::vector<int> String::SplitInts(const String &delimiter, const bool allowEmpty) const
{
    auto slices = Split(delimiter, allowEmpty);

    std::vector<int> ints = {};

    for (auto &slice : slices)
    {
        ints.push_back(slice.ToInt());
    }
    return ints;
}

String String::Join(const std::vector<String> parts) const
{
    String retval = "";
    for (auto &part : parts)
    {
        retval += part + *this;
    }
    return retval.Substring(0, retval.Length() - Length());
}

CharacterType String::CharUppercase(const CharacterType character)
{
    return FindUpperCase(character);
}

CharacterType String::CharLowercase(const CharacterType character)
{
    return FindLowerCase(character);
}

String String::ToUpper() const
{
    String upper = {};
    for (auto &chr : _data)
    {
        upper += FindUpperCase(chr);
    }

    return upper;
}

String String::ToLower() const
{
    String lower = {};
    for (auto &chr : _data)
    {
        lower += FindLowerCase(chr);
    }

    return lower;
}

String String::Left(const ssize_t length) const
{
    ssize_t actualLength = length;
    if (actualLength < 0)
    {
        actualLength += Length();
    }

    if (actualLength <= 0)
    {
        return "";
    }
    else if (actualLength >= Length())
    {
        return *this;
    }

    return Substring(0, actualLength);
}

String String::Right(const ssize_t length) const
{
    ssize_t actualLength = length;
    if (actualLength < 0)
    {
        actualLength += Length();
    }

    if (actualLength <= 0)
    {
        return "";
    }
    else if (actualLength >= Length())
    {
        return *this;
    }

    return Substring(Length() - actualLength, actualLength);
}

String String::Indent(const String &prefix) const
{
    return Replace("\n", "\n" + prefix).Replace("\n" + prefix + "\n", "\n\n");
}

String String::Dedent() const
{
    auto slices = Split("\n");
    size_t minIndent = SIZE_MAX;
    for (auto &slice : slices)
    {
        if (slice.Length() > 0U)
        {
            auto indent = static_cast<size_t>(std::find_if(slice._data.begin(), slice._data.end(), ::IsNotSpace<CharacterType>) - slice._data.begin());
            if (indent > 0U)
            {
                minIndent = std::min(minIndent, indent);
            }
        }
    }

    std::vector<String> dedentedSlices = {};
    for (auto &slice : slices)
    {
        if (slice.Length() > 0U)
        {
            auto indent = static_cast<size_t>(std::find_if(slice._data.begin(), slice._data.end(), ::IsNotSpace<CharacterType>) - slice._data.begin());
            if (indent > 0U)
            {
                dedentedSlices.push_back(slice.Substring(minIndent));
            }
        }
    }

    return String("\n").Join(dedentedSlices);
}

String String::StripEdges(const bool left, const bool right) const
{
    const size_t length = Length();
    size_t begin = 0U;
    size_t end = length;

    if (left)
    {
        for (size_t i = 0U; i < length; ++i)
        {
            if (IsNotVisible(Get(i)))
            {
                begin += 1U;
            }
            else
            {
                break;
            }
        }
    }

    if (right)
    {
        for (size_t i = length - 1U; i >= 0U; --i)
        {
            if (IsNotVisible(Get(i)))
            {
                end -= 1U;
            }
            else
            {
                break;
            }
        }
    }

    if ((begin == 0U) && (end == length))
    {
        return *this;
    }

    return Substring(begin, end - begin);
}

String String::StripEscapes() const
{
    String newString = {};
    for (auto &chr : _data)
    {
        if (IsVisible(chr))
        {
            newString += chr;
        }
    }
    return newString;
}

String String::LeftStrip(const String &characters) const
{
    auto length = Length();
    size_t pos = 0U;

    for (; pos < length; ++pos)
    {
        if (characters.FindCharacter(Get(pos)) == -1)
        {
            break;
        }
    }

    return Substring(pos, length - pos);
}

String String::RightStrip(const String &characters) const
{
    size_t pos = Length() - 1U;

    for (; pos >= 0U; --pos)
    {
        if (characters.FindCharacter(Get(pos)) == -1)
        {
            break;
        }
    }

    return Substring(0, pos + 1U);
}

String String::GetExtension() const
{
    auto dotPosition = ReverseFind(".");
    if ((dotPosition < 0) || (dotPosition < std::max(ReverseFind("/"), ReverseFind("\\"))))
    {
        return "";
    }

    return Substring(dotPosition + 1U, Length());
}

String String::GetBasename() const
{
    auto dotPosition = ReverseFind(".");
    if ((dotPosition < 0) || (dotPosition < std::max(ReverseFind("/"), ReverseFind("\\"))))
    {
        return *this;
    }

    return Substring(0, dotPosition);
}

String String::PathJoin(const String &filePath) const
{
    if (IsEmpty())
    {
        return filePath;
    }

    static const auto separator = std::filesystem::path::preferred_separator;
    if ((Get(-1) == separator) || ((filePath.Length() > 0) && (filePath[0] == separator)))
    {
        return *this + filePath;
    }

    return *this + separator + filePath;
}

std::string String::Ascii() const
{
    return std::string();
}

std::u8string String::Utf8() const
{
    return std::u8string();
}

bool String::ParseUtf8(const uint8_t *const data, const ssize_t length, const bool skipCr)
{
    return false;
}

String String::Utf8(const uint8_t *const data, const ssize_t length)
{
    return String();
}

std::u16string String::Utf16() const
{
    return std::u16string();
}

bool String::ParseUtf16(const char16_t *data, const ssize_t length)
{
    return false;
}

String String::Utf16(const char16_t *data, const ssize_t length)
{
    return String();
}

uint32_t String::Hash(const char *string)
{
    return static_cast<uint32_t>(std::hash<std::string>()(std::string(string)));
}

uint32_t String::Hash(const char *string, const ssize_t length)
{
    return static_cast<uint32_t>(std::hash<std::string>()(std::string(string, length)));
}

uint32_t String::Hash(const wchar_t *string)
{
    return static_cast<uint32_t>(std::hash<std::wstring>()(std::wstring(string)));
}

uint32_t String::Hash(const wchar_t *string, const ssize_t length)
{
    return static_cast<uint32_t>(std::hash<std::wstring>()(std::wstring(string, length)));
}

uint32_t String::Hash(const CharacterType *string)
{
    return static_cast<uint32_t>(std::hash<StringType>()(StringType(string)));
}

uint32_t String::Hash(const CharacterType *string, const ssize_t length)
{
    return static_cast<uint32_t>(std::hash<StringType>()(StringType(string, length)));
}

uint32_t String::Hash() const
{
    return static_cast<uint32_t>(std::hash<StringType>()(_data));
}

uint64_t String::Hash64() const
{
    return std::hash<StringType>()(_data);
}

String String::Md5() const
{
    return String();
}

String String::Sha1() const
{
    return String();
}

String String::Sha256() const
{
    return String();
}

std::vector<uint8_t> String::Md5ToBuffer() const
{
    return std::vector<uint8_t>();
}

std::vector<uint8_t> String::Sha1ToBuffer() const
{
    return std::vector<uint8_t>();
}

std::vector<uint8_t> String::Sha256ToBuffer() const
{
    return std::vector<uint8_t>();
}

bool String::IsEmpty() const
{
    return Length() == 0U;
}

bool String::Contains(const char *data) const
{
    return Find(data) != String::npos;
}

bool String::Contains(const String &data) const
{
    return Find(data) != String::npos;
}

bool String::IsAbsolutePath() const
{
    static const auto separator = std::filesystem::path::preferred_separator;

    if (Length() > 1U)
    {
        return (Get(0U) == separator) || (Find(":" + separator) == String::npos);
    }
    else if (Length() == 1U)
    {
        return (Get(0U) == separator);
    }
    else
    {
        return false;
    }
}

bool String::IsRelativePath() const
{
    return !IsAbsolutePath();
}

// String String::GetBaseDir() const
// {
// 	return String();
// }

// String String::GetFile() const
// {
// 	return String();
// }

// String String::HumanizeSize(const uint64_t size)
// {
// 	return String();
// }

// String String::SimplifyPath() const
// {
// 	return String();
// }

// bool String::IsNetworkSharePath() const
// {
// 	return false;
// }

// String String::XmlEscape(const bool escapeQuotes) const
// {
// 	return String();
// }

// String String::XmlUnescape() const
// {
// 	return String();
// }

// String String::UriEncode() const
// {
// 	return String();
// }

// String String::UriDecode() const
// {
// 	return String();
// }

// String String::CEscape() const
// {
// 	return String();
// }

// String String::CEscapeMultiline() const
// {
// 	return String();
// }

// String String::CUnescape() const
// {
// 	return String();
// }

// String String::JsonEscape() const
// {
// 	return String();
// }

// bool String::ParseUrl(const String &scheme, const String &host, const uint16_t &port, const String &path) const
// {
// 	return false;
// }

// String String::PropertyNameEncode() const
// {
// 	return String();
// }

bool String::IsValidInt() const
{
    const size_t length = Length();

    if (length == 0U)
    {
        return false;
    }

    size_t from = 0U;
    if ((length > 1U) && ((Get(0) == '-') || (Get(0) == '+')))
    {
        from += 1U;
    }

    return std::count_if(_data.begin() + from, _data.end(), ::IsNotDigit<CharacterType>);
}

bool String::IsValidFloat() const
{
    try
    {
        // TODO: quite invalid if the string holds a number out of range; still a valid float, maybe just not convertable
        std::stod(ConvertStringTypes<CharacterType, char>(_data));
    }
    catch (std::invalid_argument &e)
    {
        return false;
    }

    return true;
}

bool String::IsValidHexNumber(const bool withPrefix) const
{
    if (Length() == 0U)
    {
        return false;
    }

    String hexString = *this;
    if ((hexString[0] == '-') || (hexString[0] == '+'))
    {
        hexString = hexString.Substring(1U);
    }

    if (withPrefix)
    {
        if ((hexString.Length() < 3U) || (!hexString.BeginsWith("0x")))
        {
            return false;
        }
        hexString = hexString.Substring(2U);
    }

    for (auto &chr : hexString._data)
    {
        if (!IsHexDigit(chr))
        {
            return false;
        }
    }

    return true;
}

// bool String::IsValidHtmlColor() const
// {
// 	return false;
// }

// bool String::IsValidIpAddress() const
// {
// 	return false;
// }

bool String::IsValidFilename() const
{
    static const auto invalidFilenameCharacters = String(": / \\ ? * \" | % < >").SplitSpaces();

    const auto stripped = StripEdges();
    if ((stripped.IsEmpty()) || (*this != stripped))
    {
        return false;
    }

    for (const auto &chr : invalidFilenameCharacters)
    {
        if (Contains(chr))
        {
            return false;
        }
    }

    return true;
}

String::String()
{
}

String::String(const String &other)
{
    _data = other._data;
}

String &String::operator=(const String &other)
{
    _data = other._data;
    return *this;
}

std::vector<uint8_t> String::ToAsciiBuffer() const
{
    auto ascii = Ascii();
    std::vector<uint8_t> retval = {};
    std::copy(ascii.begin(), ascii.end(), back_inserter(retval));
    return retval;
}

std::vector<uint8_t> String::ToUtf8Buffer() const
{
    auto utf8 = Utf8();
    std::vector<uint8_t> retval = {};
    std::copy(utf8.begin(), utf8.end(), back_inserter(retval));
    return retval;
}

std::vector<uint8_t> String::ToUtf16Buffer() const
{
    auto utf16 = Utf16();
    std::vector<uint8_t> retval = {};
    std::copy(reinterpret_cast<uint8_t *>(utf16.data()), reinterpret_cast<uint8_t *>(utf16.data()) + utf16.length() * sizeof(char16_t), back_inserter(retval));
    return retval;
}

std::vector<uint8_t> String::ToUtf32Buffer() const
{
    auto utf32 = std::u32string(_data);
    std::vector<uint8_t> retval = {};
    std::copy(reinterpret_cast<uint8_t *>(utf32.data()), reinterpret_cast<uint8_t *>(utf32.data()) + utf32.length() * sizeof(char32_t), back_inserter(retval));
    return retval;
}

std::vector<uint8_t> String::ToWcharBuffer() const
{
    return ToUtf16Buffer();
}

String::String(const char *other)
{
    _CopyFrom(other);
}

String::String(const wchar_t *other)
{
    _CopyFrom(other);
}

String::String(const CharacterType *other)
{
    _CopyFrom(other);
}

String::String(const std::string &other)
{
    _CopyFrom(other);
}

String::String(const StringType &other)
{
    _CopyFrom(other);
}

String::String(const char *other, const size_t maxLength)
{
    _CopyFrom(other, maxLength);
}

String::String(const wchar_t *other, const size_t maxLength)
{
    _CopyFrom(other, maxLength);
}

String::String(const CharacterType *other, const size_t maxLength)
{
    _CopyFrom(other, maxLength);
}

String::String(const std::string &other, const size_t maxLength)
{
    _CopyFrom(other, maxLength);
}

String::String(const StringType &other, const size_t maxLength)
{
    _CopyFrom(other, maxLength);
}

String::operator std::string() const
{
    return ConvertStringTypes<CharacterType, char>(_data);
}

void String::_CopyFrom(const char *from)
{
    _data = ConvertStringTypes<char, CharacterType>(std::string(from));
}

void String::_CopyFrom(const char *from, const size_t maxLength)
{
    _data = ConvertStringTypes<char, CharacterType>(std::string(from, maxLength));
}

void String::_CopyFrom(const wchar_t *from)
{
    _data = ConvertStringTypes<wchar_t, CharacterType>(std::wstring(from));
}

void String::_CopyFrom(const wchar_t *from, const size_t maxLength)
{
    _data = ConvertStringTypes<wchar_t, CharacterType>(std::wstring(from, maxLength));
}

void String::_CopyFrom(const CharacterType *from)
{
    _data = std::basic_string<CharacterType>(from);
}

void String::_CopyFrom(const CharacterType *from, const size_t maxLength)
{
    _data = std::basic_string<CharacterType>(from, maxLength);
}

void String::_CopyFrom(const std::string &from)
{
    _data = ConvertStringTypes<char, CharacterType>(from);
}

void String::_CopyFrom(const std::string &from, const size_t maxLength)
{
    _data = ConvertStringTypes<char, CharacterType>(from.substr(maxLength));
}

void String::_CopyFrom(const StringType &from)
{
    _data = from;
}

void String::_CopyFrom(const StringType &from, const size_t maxLength)
{
    _data = from.substr(maxLength);
}

void String::_CopyFrom(const CharacterType &character)
{
    _data = character;
}

String String::_CamelcaseToUnderscore() const
{
    String new_string;
    size_t startIndex = 0;
    size_t size = Size();

    for (size_t i = 1U; i < size; ++i)
    {
        bool isPreviousUpper = IsAsciiUpperCase<>(_data[i - 1]);
        bool isPreviousLower = IsAsciiLowerCase<>(_data[i - 1]);
        bool isPreviousDigit = IsDigit<>(_data[i - 1]);

        bool isCurrentUpper = IsAsciiUpperCase<>(_data[i]);
        bool isCurrentLower = IsAsciiLowerCase<>(_data[i]);
        bool isCurrentDigit = IsDigit<>(_data[i]);

        bool isNextLower = false;
        if ((i + 1U) < size)
        {
            isNextLower = IsAsciiLowerCase<>(_data[i + 1]);
        }

        const bool cond1 = isPreviousLower && isCurrentUpper;                                     // aA
        const bool cond2 = (isPreviousUpper || isPreviousDigit) && isCurrentUpper && isNextLower; // AAa, 2Aa
        const bool cond3 = isPreviousDigit && isCurrentLower && isNextLower;                      // 2aa
        const bool cond4 = (isPreviousUpper || isPreviousLower) && isCurrentDigit;                // A2, a2

        if (cond1 || cond2 || cond3 || cond4)
        {
            new_string += Substring(startIndex, i - startIndex) + "_";
            startIndex = i;
        }
    }

    new_string += Substring(startIndex, size - startIndex);
    return new_string.ToLower();
}

bool operator==(const char *characterString, const String &string)
{
    return string == String(ConvertStringTypes<char, CharacterType>(std::string(characterString)));
}

bool operator==(const wchar_t *characterString, const String &string)
{
    return string == String(ConvertStringTypes<wchar_t, CharacterType>(std::wstring(characterString)));
}

bool operator!=(const char *characterString, const String &string)
{
    return string != String(ConvertStringTypes<char, CharacterType>(std::string(characterString)));
}

bool operator!=(const wchar_t *characterString, const String &string)
{
    return string != String(ConvertStringTypes<wchar_t, CharacterType>(std::wstring(characterString)));
}

String operator+(const char *characterString, const String &string)
{
    return string + String(ConvertStringTypes<char, CharacterType>(std::string(characterString)));
}

String operator+(const wchar_t *characterString, const String &string)
{
    return string + String(ConvertStringTypes<wchar_t, CharacterType>(std::wstring(characterString)));
}

String operator+(CharacterType character, const String &string)
{
    return string + character;
}

String itos(const int64_t value)
{
    return String::FromInt64(value);
}

String uitos(const uint64_t value)
{
    return String::FromUint64(value);
}

String rtos(const double value)
{
    return String::FromDouble(value);
}

String rtoss(const double value)
{
    return String::FromDoubleScientific(value);
}