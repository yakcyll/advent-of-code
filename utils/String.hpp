#ifndef STRING_HPP_
#define STRING_HPP_

#include <codecvt>
#include <cstdint>
#include <locale>
#include <string>
#include <vector>

using ssize_t = int64_t;

class String
{
public:
    using CharacterType = char32_t;
    using StringType = std::basic_string<CharacterType>;

    enum
    {
        npos = -1 ///< for "some" compatibility with std::string (npos is a
                  ///< huge value in std::string)
    };

    CharacterType *Ptr();

    void RemoveAt(const size_t index);

    void Clear();

    [[nodiscard]] CharacterType Get(const ssize_t index) const;
    void Set(const size_t index, const CharacterType &elem);
    [[nodiscard]] size_t Size() const;
    void Resize(const size_t newSize);

    [[nodiscard]] const CharacterType &operator[](const size_t index) const;

    [[nodiscard]] bool operator==(const String &other) const;
    [[nodiscard]] bool operator!=(const String &other) const;
    [[nodiscard]] String operator+(const String &other) const;
    [[nodiscard]] String operator+(const char32_t p_char) const;

    String &operator+=(const String &other);
    String &operator+=(const char32_t character);
    String &operator+=(const char *other);
    String &operator+=(const wchar_t *other);
    String &operator+=(const char32_t *other);
    String &operator+=(const std::string &other);
    String &operator+=(const StringType &other);

    String &operator=(const char *other);
    String &operator=(const wchar_t *other);
    String &operator=(const char32_t *other);
    String &operator=(const std::string &other);
    String &operator=(const StringType &other);

    [[nodiscard]] bool operator==(const char *other) const;
    [[nodiscard]] bool operator==(const wchar_t *other) const;
    [[nodiscard]] bool operator==(const char32_t *other) const;
    [[nodiscard]] bool operator==(const std::string &other) const;
    [[nodiscard]] bool operator==(const StringType &other) const;

    [[nodiscard]] bool operator!=(const char *other) const;
    [[nodiscard]] bool operator!=(const wchar_t *other) const;
    [[nodiscard]] bool operator!=(const char32_t *other) const;
    [[nodiscard]] bool operator!=(const std::string &other) const;
    [[nodiscard]] bool operator!=(const StringType &other) const;

    [[nodiscard]] bool operator<(const char *other) const;
    [[nodiscard]] bool operator<(const wchar_t *other) const;
    [[nodiscard]] bool operator<(const char32_t *other) const;
    [[nodiscard]] bool operator<(const std::string &other) const;
    [[nodiscard]] bool operator<(const StringType &other) const;

    [[nodiscard]] bool operator<(const String &other) const;
    [[nodiscard]] bool operator<=(const String &other) const;
    [[nodiscard]] bool operator>(const String &other) const;
    [[nodiscard]] bool operator>=(const String &other) const;

    [[nodiscard]] signed char
    Compare(const String &other, const bool caseInsensitive = false) const;

    [[nodiscard]] const CharacterType *Data() const;

    [[nodiscard]] size_t Length() const;

    [[nodiscard]] bool IsValidString() const;

    [[nodiscard]] void PrintUnicodeError(const String &message,
                                         const bool critical = false) const;

    [[nodiscard]] size_t Count(const String &string,
                               const size_t from = 0U,
                               const ssize_t to = -1,
                               const bool caseInsensitive = false) const;
    [[nodiscard]] String Substring(const size_t from,
                                   const ssize_t count = -1) const;
    [[nodiscard]] ssize_t Find(const String &other,
                               const size_t from = 0U,
                               const bool caseInsensitive = false) const;
    [[nodiscard]] ssize_t Find(const char *other,
                               const size_t from = 0U) const;
    [[nodiscard]] ssize_t FindCharacter(const CharacterType &character,
                                        const size_t from = 0U) const;
    [[nodiscard]] ssize_t ReverseFind(const String &other,
                                      const ssize_t from = -1,
                                      const bool caseInsensitive = false) const;
    [[nodiscard]] bool Match(const String &pattern,
                             const bool caseInsensitive) const;
    [[nodiscard]] bool BeginsWith(const String &other) const;
    [[nodiscard]] bool BeginsWith(const char *other) const;
    [[nodiscard]] bool EndsWith(const String &other) const;
    [[nodiscard]] bool IsEnclosedIn(const String &other) const;
    [[nodiscard]] bool IsQuoted() const;
    [[nodiscard]] String ReplaceFirst(const String &what,
                                      const String &with) const;
    [[nodiscard]] String Replace(const String &what,
                                 const String &with,
                                 const bool caseInsensitive = false) const;
    [[nodiscard]] String Replace(const char *what, const char *with) const;
    [[nodiscard]] String Repeat(const size_t count) const;
    [[nodiscard]] String Reverse() const;
    [[nodiscard]] String Insert(const size_t position,
                                const String &string) const;
    [[nodiscard]] String Erase(const size_t position,
                               const size_t count = 1) const;
    [[nodiscard]] String PadDecimals(const size_t digits) const;
    [[nodiscard]] String PadZeros(const size_t digits) const;
    [[nodiscard]] String TrimPrefix(const String &prefix) const;
    [[nodiscard]] String TrimSuffix(const String &suffix) const;
    [[nodiscard]] String LeftPad(const size_t minLength,
                                 const String &character = " ") const;
    [[nodiscard]] String RightPad(const size_t minLength,
                                  const String &character = " ") const;
    [[nodiscard]] String Quote(const String &quoteCharacter = "\"") const;
    [[nodiscard]] String Unquote() const;
    static String FromDouble(const double number,
                             const ssize_t decimals = -1);
    static String FromDoubleScientific(const double number);
    static String FromInt64(const int64_t number,
                            const size_t base = 10U,
                            const bool capitalizeHex = false);
    static String FromUint64(const uint64_t number,
                             const size_t base = 10U,
                             const bool capitalizeHex = false);
    static String FromChar32(const CharacterType character);
    static String HexEncodeFromBuffer(const uint8_t *buffer,
                                      const size_t length);
    [[nodiscard]] std::vector<uint8_t> HexDecode() const;

    [[nodiscard]] bool IsNumeric() const;

    [[nodiscard]] double ToFloat() const;
    [[nodiscard]] int64_t HexToInt() const;
    [[nodiscard]] int64_t BinToInt() const;
    [[nodiscard]] int64_t ToInt() const;

    static int64_t ToInt(const char *string, const ssize_t length = -1);
    static int64_t ToInt(const wchar_t *string, const ssize_t length = -1);
    static int64_t ToInt(const CharacterType *string,
                         const ssize_t length = -1,
                         const bool clamp = false);

    static double ToFloat(const char *string, const ssize_t length = -1);
    static double ToFloat(const wchar_t *string, const ssize_t length = -1);
    static double ToFloat(const CharacterType *string,
                          const ssize_t length = -1);

    [[nodiscard]] String Capitalize() const;
    [[nodiscard]] String ToCamelCase() const;
    [[nodiscard]] String ToPascalCase() const;
    [[nodiscard]] String ToSnakeCase() const;

    [[nodiscard]] size_t GetSliceCount(const String delimiter) const;
    [[nodiscard]] String GetSlice(const String delimiter,
                                  const size_t sliceIdx) const;
    [[nodiscard]] String GetSliceByCharacter(const CharacterType delimiter,
                                             const size_t sliceIdx) const;

    [[nodiscard]] std::vector<String> Split(const String &delimiter = "",
                                            const bool allowEmpty = true,
                                            const size_t maxSplitCount = SIZE_MAX) const;
    [[nodiscard]] std::vector<String>
    ReverseSplit(const String &delimiter = "",
                 const bool allowEmpty = true,
                 const size_t maxSplitCount = 0U) const;
    [[nodiscard]] std::vector<String> SplitSpaces() const;
    [[nodiscard]] std::vector<double> SplitFloats(const String &delimiter,
                                                  const bool allowEmpty = true) const;
    [[nodiscard]] std::vector<int>
    SplitInts(const String &delimiter, const bool allowEmpty = true) const;

    [[nodiscard]] String Join(const std::vector<String> parts) const;

    static CharacterType CharUppercase(const CharacterType character);
    static CharacterType CharLowercase(const CharacterType character);
    [[nodiscard]] String ToUpper() const;
    [[nodiscard]] String ToLower() const;

    [[nodiscard]] String Left(const ssize_t length) const;
    [[nodiscard]] String Right(const ssize_t length) const;
    [[nodiscard]] String Indent(const String &prefix) const;
    [[nodiscard]] String Dedent() const;
    [[nodiscard]] String StripEdges(const bool left = true,
                                    const bool right = true) const;
    [[nodiscard]] String StripEscapes() const;
    [[nodiscard]] String LeftStrip(const String &characters) const;
    [[nodiscard]] String RightStrip(const String &characters) const;
    [[nodiscard]] String GetExtension() const;
    [[nodiscard]] String GetBasename() const;
    [[nodiscard]] String PathJoin(const String &filePath) const;

    [[nodiscard]] std::string Ascii() const;
    [[nodiscard]] std::u8string Utf8() const;
    bool ParseUtf8(const uint8_t *const data,
                   const ssize_t length = -1,
                   const bool skipCr = false);
    static String Utf8(const uint8_t *const data,
                       const ssize_t length = -1);

    [[nodiscard]] std::u16string Utf16() const;
    bool ParseUtf16(const char16_t *data, const ssize_t length = -1);
    static String Utf16(const char16_t *data, const ssize_t length = -1);

    static uint32_t Hash(const CharacterType *string, const ssize_t length);
    static uint32_t Hash(const CharacterType *string);
    static uint32_t Hash(const wchar_t *string, const ssize_t length);
    static uint32_t Hash(const wchar_t *string);
    static uint32_t Hash(const char *string, const ssize_t length);
    static uint32_t Hash(const char *string);
    [[nodiscard]] uint32_t Hash() const;
    [[nodiscard]] uint64_t Hash64() const;
    [[nodiscard]] String Md5() const;
    [[nodiscard]] String Sha1() const;
    [[nodiscard]] String Sha256() const;
    [[nodiscard]] std::vector<uint8_t> Md5ToBuffer() const;
    [[nodiscard]] std::vector<uint8_t> Sha1ToBuffer() const;
    [[nodiscard]] std::vector<uint8_t> Sha256ToBuffer() const;

    [[nodiscard]] bool IsEmpty() const;
    [[nodiscard]] bool Contains(const char *data) const;
    [[nodiscard]] bool Contains(const String &data) const;

    [[nodiscard]] bool IsAbsolutePath() const;
    [[nodiscard]] bool IsRelativePath() const;
    // [[nodiscard]] String PathTo(const String &path) const;
    // [[nodiscard]] String PathToFile(const String &path) const;
    // [[nodiscard]] String GetBaseDir() const;
    // [[nodiscard]] String GetFile() const;
    // [[nodiscard]] static String HumanizeSize(const uint64_t size);
    // [[nodiscard]] String SimplifyPath() const;
    // [[nodiscard]] bool IsNetworkSharePath() const;

    // [[nodiscard]] String XmlEscape(const bool escapeQuotes = false)
    // const; [[nodiscard]] String XmlUnescape() const; [[nodiscard]] String
    // UriEncode() const; [[nodiscard]] String UriDecode() const;
    // [[nodiscard]] String CEscape() const;
    // [[nodiscard]] String CEscapeMultiline() const;
    // [[nodiscard]] String CUnescape() const;
    // [[nodiscard]] String JsonEscape() const;
    // [[nodiscard]] bool ParseUrl(const String &scheme, const String &host,
    // const uint16_t &port, const String &path) const;

    // String PropertyNameEncode() const;

    [[nodiscard]] bool IsValidInt() const;
    [[nodiscard]] bool IsValidFloat() const;
    [[nodiscard]] bool IsValidHexNumber(const bool withPrefix = true) const;
    // [[nodiscard]] bool IsValidHtmlColor() const;
    // [[nodiscard]] bool IsValidIpAddress() const;
    [[nodiscard]] bool IsValidFilename() const;

    /**
     * The constructors must not depend on other overloads
     */

    String();
    String(const String &other);
    String &operator=(const String &other);

    [[nodiscard]] std::vector<uint8_t> ToAsciiBuffer() const;
    [[nodiscard]] std::vector<uint8_t> ToUtf8Buffer() const;
    [[nodiscard]] std::vector<uint8_t> ToUtf16Buffer() const;
    [[nodiscard]] std::vector<uint8_t> ToUtf32Buffer() const;
    [[nodiscard]] std::vector<uint8_t> ToWcharBuffer() const;

    String(const char *other);
    String(const wchar_t *other);
    String(const CharacterType *other);
    String(const std::string &other);
    String(const StringType &other);
    String(const char *other, const size_t maxLength);
    String(const wchar_t *other, const size_t maxLength);
    String(const CharacterType *other, const size_t maxLength);
    String(const std::string &other, const size_t maxLength);
    String(const StringType &other, const size_t maxLength);

    [[nodiscard]] operator std::string() const;

private:
    StringType _data;
    static inline const CharacterType _null{0};
    static inline const CharacterType _replacementCharacter{
        0xFFFD}; // Unicode <?> character

    void _CopyFrom(const char *from);
    void _CopyFrom(const char *from, const size_t maxLength);
    void _CopyFrom(const wchar_t *from);
    void _CopyFrom(const wchar_t *from, const size_t maxLength);
    void _CopyFrom(const CharacterType *from);
    void _CopyFrom(const CharacterType *from, const size_t maxLength);
    void _CopyFrom(const std::string &from);
    void _CopyFrom(const std::string &from, const size_t maxLength);
    void _CopyFrom(const StringType &from);
    void _CopyFrom(const StringType &from, const size_t maxLength);
    void _CopyFrom(const CharacterType &character);

    String _CamelcaseToUnderscore() const;
};

template <>
struct std::hash<String>
{
    uint64_t operator()(const String &x) const { return x.Hash64(); }
};

bool operator==(const char *characterString, const String &string);
bool operator==(const wchar_t *characterString, const String &string);
bool operator!=(const char *characterString, const String &string);
bool operator!=(const wchar_t *characterString, const String &string);

String operator+(const char *characterString, const String &string);
String operator+(const wchar_t *characterString, const String &string);
String operator+(String::CharacterType character, const String &string);

String itos(const int64_t value);
String uitos(const uint64_t value);
String rtos(const double value);
String rtoss(const double value); // scientific version

template <typename SourceT, typename DestT>
std::basic_string<DestT>
ConvertStringTypes(const std::basic_string<SourceT> &source)
{
    // std::locale::global(std::locale("en-GB"));
    // auto &f = std::use_facet<std::codecvt<SourceT, DestT,
    // std::mbstate_t>>(std::locale());

    // std::mbstate_t mb{};
    // std::basic_string<DestT> output(source.size() * f.max_length(),
    // '\0'); const SourceT *from_next; DestT *to_next;

    // f.out(mb,
    // 	  &source[0], &source[source.size()], from_next,
    // 	  &output[0], &output[output.size()], to_next);
    // output.resize(to_next - &output[0]);

    // return output;

    if constexpr (sizeof(SourceT) > sizeof(DestT))
    {
        if constexpr (sizeof(DestT) == 1U)
        {
            std::wstring_convert<std::codecvt_utf8<SourceT>, SourceT> conv;
            return conv.to_bytes(source);
        }
        else if constexpr (sizeof(DestT) == 2U)
        {
            std::wstring_convert<std::codecvt_utf16<SourceT>, SourceT> conv;
            std::string bytes = conv.to_bytes(source);
            return std::basic_string<DestT>(
                reinterpret_cast<const DestT *>(bytes.c_str()),
                bytes.length());
        }
    }
    else if constexpr (sizeof(SourceT) < sizeof(DestT))
    {
        if constexpr (sizeof(SourceT) == 1U)
        {
            std::wstring_convert<std::codecvt_utf8<DestT>, DestT> conv;
            return conv.from_bytes(source);
        }
        else if constexpr (sizeof(SourceT) == 2U)
        {
            const SourceT *data = source.c_str();
            std::wstring_convert<std::codecvt_utf16<DestT>, DestT> conv;
            return conv.from_bytes(
                reinterpret_cast<const char *>(data),
                reinterpret_cast<const char *>(data + source.length()));
        }
    }
    else
    {
        return source;
    }
}

static inline std::vector<std::string>
ConvertStringVectorToStdStringVector(const std::vector<String> &strings)
{
    std::vector<std::string> retval = {};
    for (auto &str : strings)
    {
        retval.push_back(
            ConvertStringTypes<String::CharacterType, char>(str.Data()));
    }

    return retval;
}

struct NoCaseComparator
{
    bool operator()(const String &a, const String &b) const
    {
        return a.Compare(b, true) < 0;
    }
};

#endif // STRING_HPP_
