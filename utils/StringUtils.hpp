#ifndef STRING_UTILS_HPP_
#define STRING_UTILS_HPP_

template <typename T>
static inline bool IsDigit(const T chr)
{
    return (chr >= '0') && (chr <= '9');
}

template <typename T>
static inline bool IsNotDigit(const T chr)
{
    return !IsDigit<>(chr);
}

template <typename T>
static inline bool IsHex(const T chr)
{
    return ((chr >= 'a') && (chr <= 'f')) || ((chr >= 'A') && (chr <= 'F'));
}

template <typename T>
static inline bool IsNotHex(const T chr)
{
    return !IsHex<>(chr);
}

template <typename T>
static inline bool IsHexDigit(const T chr)
{
    return IsHex<>(chr) || IsDigit<>(chr);
}

template <typename T>
static inline bool IsBinaryDigit(const T chr)
{
    return (chr == '0') || (chr == '1');
}

template <typename T>
static inline bool IsNotBinaryDigit(const T chr)
{
    return !IsBinaryDigit<>(chr);
}

template <typename T>
static inline bool IsAsciiUpperCase(const T chr)
{
    return ((chr >= 'A') && (chr <= 'Z'));
}

template <typename T>
static inline bool IsAsciiLowerCase(const T chr)
{
    return ((chr >= 'a') && (chr <= 'z'));
}

template <typename T>
static inline bool IsSpace(const T chr)
{
    return (chr == ' ');
}

template <typename T>
static inline bool IsNotSpace(const T chr)
{
    return (chr != ' ');
}

template <typename T>
static inline bool IsVisible(const T chr)
{
    return (chr > 32);
}

template <typename T>
static inline bool IsNotVisible(const T chr)
{
    return (chr <= 32);
}

#endif // STRING_UTILS_HPP_