#include <algorithm>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "String.hpp"

int main()
{
    std::ifstream infile("2.in", std::ios_base::in);

    uint64_t sum = 0U;
    std::string line;

    while (std::getline(infile, line))
    {
        String sline(line);
        const auto idResultsPair = sline.Split(":");

        const auto results = idResultsPair[1].StripEdges().Split(";");
        std::vector<int> minimums = {0U, 0U, 0U};

        for (const auto &result : results)
        {
            const auto colors = result.Split(",");
            for (const auto &color : colors)
            {
                auto countColorPair = color.StripEdges().SplitSpaces();
                if (countColorPair[1] == "red")
                {
                    minimums[0] = std::max(minimums[0], std::stoi(countColorPair[0]));
                }
                else if (countColorPair[1] == "green")
                {
                    minimums[1] = std::max(minimums[1], std::stoi(countColorPair[0]));
                }
                else if (countColorPair[1] == "blue")
                {
                    minimums[2] = std::max(minimums[2], std::stoi(countColorPair[0]));
                }
            }
        }

        const uint64_t power = minimums[0] * minimums[1] * minimums[2];
        sum += power;
    }

    std::cout << sum << std::endl;

    return 0;
}