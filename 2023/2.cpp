#include <algorithm>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "String.hpp"

// red, green, blue
const std::vector<uint64_t> limits = {12U, 13U, 14U};

int main()
{
    std::ifstream infile("2.in", std::ios_base::in);

    uint64_t sum = 0U;
    std::string line;

    std::vector<uint64_t> check(limits.size());

    while (std::getline(infile, line))
    {
        String sline(line);
        const auto idResultsPair = sline.Split(":");

        const auto id = std::stoi(idResultsPair[0].SplitSpaces()[1]);
        bool valid = true;

        const auto results = idResultsPair[1].StripEdges().Split(";");

        for (const auto &result : results)
        {
            const auto colors = result.Split(",");
            for (const auto &color : colors)
            {
                auto countColorPair = color.StripEdges().SplitSpaces();
                uint64_t limit = 0U;
                if (countColorPair[1] == "red")
                {
                    limit = limits[0];
                }
                else if (countColorPair[1] == "green")
                {
                    limit = limits[1];
                }
                else if (countColorPair[1] == "blue")
                {
                    limit = limits[2];
                }

                if (limit < std::stoi(countColorPair[0]))
                {
                    valid = false;
                    break;
                }
            }

            if (!valid)
            {
                break;
            }
        }

        if (valid)
        {
            sum += id;
        }
    }

    std::cout << sum << std::endl;

    return 0;
}