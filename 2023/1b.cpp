#include <algorithm>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <utility>

const std::vector<std::string> validNumbers = {{"one",
                                                "two",
                                                "three",
                                                "four",
                                                "five",
                                                "six",
                                                "seven",
                                                "eight",
                                                "nine",
                                                "zero",
                                                "1",
                                                "2",
                                                "3",
                                                "4",
                                                "5",
                                                "6",
                                                "7",
                                                "8",
                                                "9",
                                                "0"}};

const std::map<std::string, std::string> wordsToDigits = {
    {"one", "1"},
    {"two", "2"},
    {"three", "3"},
    {"four", "4"},
    {"five", "5"},
    {"six", "6"},
    {"seven", "7"},
    {"eight", "8"},
    {"nine", "9"},
    {"zero", "0"},
};

std::string GetDigit(const std::string &input)
{
    if (wordsToDigits.find(input) != wordsToDigits.end())
    {
        return wordsToDigits.at(input);
    }
    return input;
}

std::string FindFirstOf(const std::string &input, const std::vector<std::string> &substrs)
{
    auto firstItr = std::make_pair(input.npos, std::string());
    for (auto &substr : substrs)
    {
        const auto substrItr = input.find(substr);
        if (substrItr < firstItr.first)
        {
            firstItr = std::make_pair(substrItr, substr);
        }
    }
    return firstItr.second;
}

std::string FindLastOf(const std::string &input, const std::vector<std::string> &substrs)
{
    auto lastItr = std::make_pair(ssize_t{-1}, std::string());
    for (auto &substr : substrs)
    {
        ssize_t substrItr = input.rfind(substr);

        if (substrItr > lastItr.first)
        {
            lastItr = std::make_pair(substrItr, substr);
        }
    }
    return lastItr.second;
}

int main()
{
    std::ifstream infile("1.in", std::ios_base::in);

    uint64_t sum = 0U;
    uint64_t term = 0U;
    std::string line;

    while (infile >> line)
    {
        auto leftDigit = GetDigit(FindFirstOf(line, validNumbers));
        auto rightDigit = GetDigit(FindLastOf(line, validNumbers));
        term = std::stoi(leftDigit + rightDigit);
        sum += term;
    }

    std::cout << sum << std::endl;

    return 0;
}