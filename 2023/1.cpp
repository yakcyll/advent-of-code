#include <algorithm>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <string>

int main()
{
    std::ifstream infile("1.in", std::ios_base::in);

    uint64_t sum = 0U;
    std::string line;

    while (infile >> line)
    {
        auto leftDigit = std::string(1, *std::find_if(line.begin(), line.end(), ::isdigit));
        auto rightDigit = std::string(1, *std::find_if(line.rbegin(), line.rend(), ::isdigit));
        sum += std::stoi(leftDigit + rightDigit);
    }

    std::cout << sum << std::endl;

    return 0;
}