#include <cstdint>
#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>
#include <utility>

class TwoDeeTextMap
{
public:
    using Coord = std::pair<size_t, size_t>;
    using Coords = std::vector<Coord>;
    void LoadText(const std::string &textMap);
    Coords FindSymbolCoords();
    std::vector<uint64_t> FindNumbersAdjacentToCoords(const Coords &coords);
    uint64_t ReconstructNumberAtCoord(const Coord &coord);

    std::vector<std::vector<char>> map;
};

void TwoDeeTextMap::LoadText(const std::string &textMap)
{
    std::string::size_type startPos = 0U;
    std::string::size_type endPos = 0U;
    while ((endPos = textMap.find('\n', startPos)) != std::string::npos)
    {
        const auto line = textMap.substr(startPos, endPos - startPos);
        startPos = endPos + 1U;

        std::vector<char> mapLine(line.begin(), line.end());
        map.push_back(mapLine);
    }
}

TwoDeeTextMap::Coords TwoDeeTextMap::FindSymbolCoords()
{
    Coords coords = {};

    for (size_t i = 0U; i < map.size(); ++i)
    {
        for (size_t j = 0U; j < map[i].size(); ++j)
        {
            if (map[i][j] == '.' || ::isdigit(map[i][j]))
            {
                continue;
            }

            coords.push_back(std::make_pair(i, j));
        }
    }

    return coords;
}

std::vector<uint64_t> TwoDeeTextMap::FindNumbersAdjacentToCoords(const Coords &coords)
{
    std::vector<uint64_t> numbers = {};

    for (const auto &coord : coords)
    {
        // Check left
        if (coord.second != 0U)
        {
            if (::isdigit(map[coord.first][coord.second - 1U]))
            {
                numbers.push_back(ReconstructNumberAtCoord({coord.first, coord.second - 1U}));
            }
        }

        // Check right
        if (coord.second != (map[0].size() - 1U))
        {
            if (::isdigit(map[coord.first][coord.second + 1U]))
            {
                numbers.push_back(ReconstructNumberAtCoord({coord.first, coord.second + 1U}));
            }
        }

        // Check top
        if (coord.first != 0U)
        {
            if (::isdigit(map[coord.first - 1U][coord.second]))
            {
                numbers.push_back(ReconstructNumberAtCoord({coord.first - 1U, coord.second}));
            }
            //   If not, check top left and top right
            else
            {
                if (coord.second != 0U)
                {
                    if (::isdigit(map[coord.first - 1U][coord.second - 1U]))
                    {
                        numbers.push_back(ReconstructNumberAtCoord({coord.first - 1U, coord.second - 1U}));
                    }
                }

                if (coord.second != (map[0].size() - 1U))
                {
                    if (::isdigit(map[coord.first - 1U][coord.second + 1U]))
                    {
                        numbers.push_back(ReconstructNumberAtCoord({coord.first - 1U, coord.second + 1U}));
                    }
                }
            }
        }

        // Check bottom
        if (coord.first != (map.size() - 1U))
        {
            if (::isdigit(map[coord.first + 1U][coord.second]))
            {
                numbers.push_back(ReconstructNumberAtCoord({coord.first + 1U, coord.second}));
            }
            //   If not, check bottom left and bottom right
            else
            {
                if (coord.second != 0U)
                {
                    if (::isdigit(map[coord.first + 1U][coord.second - 1U]))
                    {
                        numbers.push_back(ReconstructNumberAtCoord({coord.first + 1U, coord.second - 1U}));
                    }
                }

                if (coord.second != (map[0].size() - 1U))
                {
                    if (::isdigit(map[coord.first + 1U][coord.second + 1U]))
                    {
                        numbers.push_back(ReconstructNumberAtCoord({coord.first + 1U, coord.second + 1U}));
                    }
                }
            }
        }
    }

    return numbers;
}

uint64_t TwoDeeTextMap::ReconstructNumberAtCoord(const Coord &coord)
{
    std::string numberStr = std::string{map[coord.first][coord.second]};

    if (coord.second != 0U)
    {
        // Go left
        size_t digitIdx = coord.second - 1U;
        while (::isdigit(map[coord.first][digitIdx]))
        {
            numberStr = std::string{map[coord.first][digitIdx]} + numberStr;
            if (digitIdx == 0U)
            {
                break;
            }
            digitIdx -= 1U;
        }
    }
    if (coord.second != (map[0].size() - 1U))
    {
        // Go right
        size_t digitIdx = coord.second + 1U;
        while (::isdigit(map[coord.first][digitIdx]))
        {
            numberStr = numberStr + std::string{map[coord.first][digitIdx]};
            if (digitIdx == (map[0].size() - 1U))
            {
                break;
            }
            digitIdx += 1U;
        }
    }

    return std::stoi(numberStr);
}

int main()
{
    std::string intext = "";
    std::string line = "";

    auto infile = std::ifstream("3.in", std::ios_base::in);

    while (infile >> line)
    {
        intext += line + '\n';
    }

    TwoDeeTextMap twodeemap;
    twodeemap.LoadText(intext);
    const auto numbers = twodeemap.FindNumbersAdjacentToCoords(twodeemap.FindSymbolCoords());
    const auto sum = std::accumulate(numbers.begin(), numbers.end(), 0U);

    std::cout << sum << std::endl;

    return 0;
}